// Programa para dibujar gráficas.


#include "xyplotwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    XYPlotWindow w;

    double y = 0.00;
    double x = 0.00;
    double increment = 0.01;

    for (double t = 0; t < 2*M_PI; t = t + increment)
    {

       x=t;
       y=t;

        // añadimos la x y la y como un punto más de la gráfica
        w.AddPointToGraph(x,y);
    }

    // Una vez hemos añadido todos los puntos graficamos
    w.Plot();

    w.show();

    return a.exec();
}
