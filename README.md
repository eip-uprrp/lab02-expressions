Las expresiones aritméticas son parte esencial de casi cualquier algoritmo para resolver un problema útil.  Por lo tanto, aprender a implementar correctamente expresiones aritméticas es una destreza básica en cualquier lenguaje de programación. En esta experiencia de laboratorio practicaremos la implementación de expresiones aritméticas en C++.

#Objetivos:

Al finalizar la experiencia de laboratorio de hoy los estudiantes habrán implementado expresiones aritméticas en C++ para producir gráficas y trayectorias. También habrán aprendido a escribir e implementar un programa en donde practicarán el uso de constantes y escoger, declarar, convertir y usar el tipo adecuado para las variables.


#Pre-Lab:

Antes de llegar al laboratorio cada estudiante debe:

1. haber repasado los siguientes conceptos:

	a. Implementación de expresiones aritméticas en C++

	b. Tipos de datos nativos de C++

	c. Uso del "type cast" para covertir el tipo de un número.

	d. Uso de funciones aritméticas de la biblioteca cmath

2. haber estudiado los conceptos e instrucciones para la sesión de laboratorio.

3. haber tomado el quiz Pre-Lab 2.


#Sesión de laboratorio:

##Ecuaciones paramétricas

En matemática, una *ecuación paramétrica* es una representación, casi siempre de una curva, por medio de ecuaciones que expresan las coordenadas de los puntos de la curva como ecuaciones en una variable, llamada el *parámetro*. Por ejemplo, es posible que hayas visto en tu curso de trigonometría que la ecuación de un círculo con centro en el origen tiene una forma así: 

$$x^2+y^2=r^2$$


donde $r$ es el radio del círculo. Los puntos $(x,y)$ que satisfacen esta ecuación son los puntos que forman el círculo de radio $r$ y centro en el origen.  El círculo con $r=2$ y centro en el origen tiene ecuación:

$$x^2+y^2=4,$$

y sus puntos son los pares ordenados $(x,y)$ que satisfacen esa ecuación. Una forma paramétrica de expresar las coordenadas de los puntos de un círculo es: 

$$x=r \cos(t)$$

$$y=r \sin(t)$$

donde $t$ es un parámetro que corresponde a la medida del ángulo (en radianes) que forma la parte no negativa del eje de $x$ con el rayo que comienza en el origen y pasa por el punto $(x,y)$, como se muestra en la figura de abajo. 


![](http://demo05.cloudimage.io/s/resize/232/i.imgur.com/HZj4uey.jpg)

Para graficar, computamos los valores de $x$ y $y$ para un conjunto de valores del parámetro. Por ejemplo, para $r = 2$ algunos de los valores son



| $t$ | $x$ | $y$ |
|-----|-----|-----|
| $0$ | $2$ | $0$ |
| $\frac{\pi}{4}$ | $\frac{\sqrt{2}}{2}$ | $\frac{\sqrt{2}}{2}$ |
| $\frac{\pi}{2}$ | $0$ | $2$ |





###**Ejercicio 1**

En este ejercicio graficarás algunas ecuaciones (expresadas como ecuaciones paramétricas) que generan curvas interesantes. 

**Instrucciones**

1.	Ve a la pantalla de terminal y escribe el comando `git clone https://bitbucket.org/eip-uprrp/Lab02-Expressions.git` para descargar la carpeta `Lab02-Expressions` a tu computadora.

2.	Marca doble "click" en el archivo `prettyPlot.pro` para cargar el proyecto a Qt. Configura el proyecto y ejecuta el programa marcando el botón verde en el menú de la izquierda. El programa debe mostrar algo parecido a la siguiente figura.

![](http://demo05.cloudimage.io/s/resize/272/i.imgur.com/lha1Q8g.png)

3. El archivo `main.cpp` (en Sources) contiene la función donde estarás cambiando el código. Abre ese archivo y observa el ciclo `for`. En este ciclo se generan una serie de valores para $t$ y se computa un valor de $x$ y $y$ para cada valor de $t$. Cada par ordenado $(x,y)$  es añadido a la gráfica por medio de `AddPointToGraph(x,y)`. Al final de la función se invoca la función `Plot()` que "dibuja" los puntos y `show()` muestra la gráfica. Actualmente las expresiones para $x$ y $y$ son son ecuaciones paramétricas  para la línea que pasa por el origen y tiene las mismas coordenadas en $x$ y $y$. En la [página de entregas del Lab 2](http://moodle.ccom.uprrp.edu/mod/quiz/view.php?id=6577) explica por qué la línea solo llega desde 0 hasta aproximadamente 6.

4.	Cambia las expresiones para $x$ y $y$ en la función `main.cpp` para que el programa grafique un círculo de radio 3 con centro en el origen.  Ejecuta tu programa. Al obtener la figura correcta, copia las expresiones para $x$ y $y$ que utilizaste en el programa al lugar correspondiente de la [página de entregas del Lab 2](http://moodle.ccom.uprrp.edu/mod/quiz/view.php?id=6577).

5. Ahora graficaremos una figura cuyas ecuaciones paramétricas son:

	$$x=16 \sin^3(t)$$
	$$y=13 \cos(t) - 5 \cos(2t) - 2 \cos(3t) - \cos(4t)$$


	Si implementas las expresiones correctamente debes ver un corazón graficado.  Al obtener la figura correcta, copia las expresiones para $x$ y $y$ que utilizaste en el programa al lugar correspondiente de la [página de entregas del Lab 2](http://moodle.ccom.uprrp.edu/mod/quiz/view.php?id=6577).

6. Ahora graficaremos una figura cuyas ecuaciones paramétricas son:

	$$x=5\cos(t) \left[ \sin^2(1.2t) + \cos^3(6t) \right]$$
	$$y= 10\sin(t) \left[ \sin^2(1.2t) +  \cos^3(6t) \right]$$


	Observa que ambas expresiones son iguales excepto que una comienza con $\cos(t)$ y la otra con $\sin(t)$. En lugar de realizar el cómputo de $ \sin^2(1.2t) + \cos^3(6t)$ dos veces, podemos asignar su valor a otra variable y realizar el cómputo así:

	$$q =  \sin^2(1.2t) + \cos^3(6t)$$
	$$x = 5q \cos(t)$$
	$$y = 10q  \sin(t)$$


	Cambia la condición de terminación del `for` a `t < 16*M_PI`. Implementa las expresiones de arriba y observa la gráfica que resulta. Se supone que parezca una mariposa. Al obtener la figura correcta, copia las expresiones para $x$ y $y$ que utilizaste en el programa al lugar correspondiente de la [página de entregas del Lab 2](http://moodle.ccom.uprrp.edu/mod/quiz/view.php?id=6577).


##Ejercicio 2

Supón que cada curso en la Universidad de Yauco es de $3$ créditos y que las notas tienen las siguientes puntuaciones: $A = 4$ puntos; $B = 3$ puntos; $C = 2$ puntos; $D = 1$ punto y $F = 0$ puntos. 

Escribe un programa que defina las constantes $A, B, C, D, F$ para la puntuación de las notas y lea los valores para las variables $NumA, NumB, NumC, NumD, NumF$, donde $NumA$ representará el número de cursos en los que el estudiante obtuvo $A$, donde $NumB$ representará el número de cursos en los que el estudiante obtuvo $B$, etc. El programa debe desplegar el promedio general del estudiante.

**Ayuda:** recuerda que, en C++, si divides dos números enteros el resultado se "truncará" y será un número entero. Utiliza "type casting": `static_cast\<tipo\>(número)' para resolver este problema.

Cuando finalices, copia las instrucciones de tu programa al lugar correspondiente de la [página de entregas del Lab 2](http://moodle.ccom.uprrp.edu/mod/quiz/view.php?id=6577).